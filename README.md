## Hospital Demo Application

The demo application exposing rest endpoints for getting patient, doctor, nurse information.
The application uses Java 8, Spring Boot, JPA, Maven and the in-memory database H2.
The application runs on port `8585` which is defined in the `application.properties` and for
development purpose `h2-console` has been enabled to see the sample data inserted.

### Application flow

The request mapping is defined in the controller which calls the service layer. Service layer
is responsible for returning the DTO for the JSON response. The service layer talks to the
repository layer and retrieves the entities and transformer is used to transform the entity data
back to DTO and return the same to the controller.

### Usage

#### Compile the application

`mvn clean compile`

#### Test the application

`mvn clean install`

### Docker usage

`mvn clean install` will build the docker image `hospital-demo/hospital-demo`

To build only the Docker image run the following command:

`mvn dockerfile:build`

By default it will build the docker image, but to skip that run the following command:

`mvn clean install -Dskip.docker.build=true`

#### Run the application

#### Run locally 

`mvn spring-boot:run`

#### Run the docker image

`docker run -p 8585:8585 hospital-demo/hospital-demo`

### Requests for testing

1. Get the lists of patients in a particular ward and room
    - `http://localhost:8585/wards/ward-id-1/rooms/room-2/patients`

2. Get the lists of doctors in a particular ward
    - `http://localhost:8585/wards/ward-id-2/doctors`

3. Get the lists of nurses in a particular ward
    - `http://localhost:8585/wards/ward-id-2/nurses`

4. Get the lists of operation theatres in a particular ward
    - `http://localhost:8585/wards/ward-id-1/theatres`
    
5. Get the patient chart of a particular patient
    - `http://localhost:8585/patients/patient-id-1/chart`

### Design decisions

* One of the design decisions was which database to use. The relationships between multiple entities were not
that large so I used a relational database (H2). If there were too many relationships then we can use a Graph database
as the database performance decreases with more relationships as it requires more joins.
* It would have been easy to return the entity directly back to the controller as a response, but it would be 
a bad design to expose the entity to the controller, that's why I added a service layer in between that will
transform the entity to the DTO and then it is returned by the controller.
* The request endpoints didn't require fetching child entities so the default fetching strategy is LAZY which
can result in increased performance.
* It's a good design to have different layers, such as controller which define request mappings, a service
layer which is optional and sits in between controller and repository layer, and the repository layer that
uses entity manager for transactions.

### Limitation and potential enhancements

#### Use graphQL instead of defining REST endpoints
 
Define a GraphQL endpoint instead of multiple rest endpoints. For example - Instead of having multiple request
mappings we can have a single graphQL endpoint that would have exposed the schema containing - doctor, nurses, 
wards, patients, theatres etc with a filter that can filter the data based on any of the field defined
in the schema. Spring boot project would automatically pick up all the GraphQL schemas and expose them for the
user to query.

#### Use GraphDB instead of relational database

The relationship between wards, doctors, nurses, rooms, and patients can be best described with the use of 
Graph database such as Neo4J, GraphDB etc. Each of the entity can be represented as nodes amd their relationship
can go in the edges. For ex - doctors, wards can be nodes of the graph and the relationship between them can
be represented in the edges. Doctor A is overseeing ward A and ward B, and ward A is overseen by doctor A and
doctor C.

#### Application scaling

The dockerized version of the application can be deployed in a Kubernetes cluster that can run multiple
instances of the application in multiple pods also known as horizontal pod scaling. It uses integrated load
balancer to distribute the traffic to multiple instances of applications running.
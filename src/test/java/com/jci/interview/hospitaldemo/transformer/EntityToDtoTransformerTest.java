package com.jci.interview.hospitaldemo.transformer;

import com.jci.interview.hospitaldemo.domain.*;
import com.jci.interview.hospitaldemo.model.*;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class EntityToDtoTransformerTest {
    
    private EntityToDtoTransformer entityToDtoTransformer = new EntityToDtoTransformer();

    @Test
    public void testPatientChartTransformer() {
        PatientChart patientChart = new PatientChart();
        patientChart.setChartName("test-chart-name");
        patientChart.setChartDescription("test-chart-desc");

        PatientChartDto dto = entityToDtoTransformer.transform(patientChart);

        assertThat(dto).isNotNull();

        assertThat(dto.getChartName()).isEqualTo(patientChart.getChartName());
        assertThat(dto.getChartDescription()).isEqualTo(patientChart.getChartDescription());
    }

    @Test
    public void testPatientsTransformer() {
        Patient patient = new Patient();
        patient.setPatientName("test-patient-name");
        patient.setPatientId("test-patient-id");

        List<PatientDto> patientDtos = entityToDtoTransformer.transformPatients(Collections.singletonList(patient));

        assertThat(patientDtos).isNotNull();
        assertThat(patientDtos).size().isEqualTo(1);

        PatientDto patientDto = patientDtos.get(0);

        assertThat(patientDto.getPatientId()).isEqualTo(patient.getPatientId());
        assertThat(patientDto.getPatientName()).isEqualTo(patient.getPatientName());
    }

    @Test
    public void testNursesTransformer() {
        Nurse nurse = new Nurse();
        nurse.setNurseName("test-nurse-name");
        nurse.setNurseId("test-nurse-id");

        List<NurseDto> nurseDtos = entityToDtoTransformer.transformNurses(Collections.singletonList(nurse));

        assertThat(nurseDtos).isNotNull();
        assertThat(nurseDtos).size().isEqualTo(1);

        NurseDto nurseDto = nurseDtos.get(0);

        assertThat(nurseDto.getNurseId()).isEqualTo(nurse.getNurseId());
        assertThat(nurseDto.getNurseName()).isEqualTo(nurse.getNurseName());
    }

    @Test
    public void testDoctorsTransformer() {
        Doctor doctor = new Doctor();
        doctor.setDoctorName("test-doctor-name");
        doctor.setDoctorId("test-doctor-id");

        List<DoctorDto> doctorDtos = entityToDtoTransformer.transformDoctors(Collections.singletonList(doctor));

        assertThat(doctorDtos).isNotNull();
        assertThat(doctorDtos).size().isEqualTo(1);

        DoctorDto doctorDto = doctorDtos.get(0);

        assertThat(doctorDto.getDoctorId()).isEqualTo(doctor.getDoctorId());
        assertThat(doctorDto.getDoctorName()).isEqualTo(doctor.getDoctorName());
    }

    @Test
    public void testOperationTheatresTransformer() {
        OperationTheatre operationTheatre = new OperationTheatre();
        operationTheatre.setTheatreId("test-theatre-id");
        operationTheatre.setTheatreName("test-theatre-name");

        List<OperationTheatreDto> operationTheatreDtos = entityToDtoTransformer
                .transformOperationTheatres(Collections.singletonList(operationTheatre));

        assertThat(operationTheatreDtos).isNotNull();
        assertThat(operationTheatreDtos).size().isEqualTo(1);

        OperationTheatreDto operationTheatreDto = operationTheatreDtos.get(0);

        assertThat(operationTheatreDto.getOtId()).isEqualTo(operationTheatre.getTheatreId());
        assertThat(operationTheatreDto.getOtName()).isEqualTo(operationTheatre.getTheatreName());
    }

}
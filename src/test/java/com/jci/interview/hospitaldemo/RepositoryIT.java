package com.jci.interview.hospitaldemo;

import com.jci.interview.hospitaldemo.domain.*;
import com.jci.interview.hospitaldemo.repository.HospitalRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class RepositoryIT {

    @Autowired
    private HospitalRepository hospitalRepository;

    @Test
    public void getPatientChartReturnsData() {
        PatientChart patientChart = hospitalRepository.getPatientChart("patient-id-1");

        assertThat(patientChart).isNotNull();

        assertThat(patientChart.getChartName()).isEqualTo("1-patient-chart-name");
        assertThat(patientChart.getChartDescription()).isEqualTo("1-patient-chart-description");
    }

    @Test
    public void getPatientsInRoomReturnsData() {
        List<Patient> patients = hospitalRepository.getPatientsInRoom("ward-id-1", "room-2");

        assertThat(patients).isNotNull();
        assertThat(patients).size().isEqualTo(2);
    }

    @Test
    public void getNursesInwardReturnsData() {
        List<Nurse> nurses = hospitalRepository.getNursesInWard("ward-id-1");

        assertThat(nurses).isNotNull();
        assertThat(nurses).size().isEqualTo(2);
    }

    @Test
    public void getDoctorsInwardReturnsData() {
        List<Doctor> doctors = hospitalRepository.getDoctorsInWard("ward-id-2");

        assertThat(doctors).isNotNull();
        assertThat(doctors).size().isEqualTo(4);
    }

    @Test
    public void getOperationTheatresInwardReturnsData() {
        List<OperationTheatre> theatres = hospitalRepository.getOperationTheatresInWard("ward-id-2");

        assertThat(theatres).isNotNull();
        assertThat(theatres).size().isEqualTo(2);
    }

}

package com.jci.interview.hospitaldemo.model;

import java.util.List;

public class HospitalDto {

    private String hospitalName;

    private List<WardDto> wards;

    public void addWard(WardDto ward) {
        this.getWards().add(ward);
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public List<WardDto> getWards() {
        return wards;
    }

    public void setWards(List<WardDto> wards) {
        this.wards = wards;
    }
}

package com.jci.interview.hospitaldemo.model;

public class PatientChartDto {

    private String chartDescription;
    private String chartName;

    public PatientChartDto() {
    }

    public PatientChartDto(String chartDescription, String chartName) {
        this.chartDescription = chartDescription;
        this.chartName = chartName;
    }

    public String getChartDescription() {
        return chartDescription;
    }

    public void setChartDescription(String chartDescription) {
        this.chartDescription = chartDescription;
    }

    public String getChartName() {
        return chartName;
    }

    public void setChartName(String chartName) {
        this.chartName = chartName;
    }
}

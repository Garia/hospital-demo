package com.jci.interview.hospitaldemo.model;

public class OperationTheatreDto {

    private String otId;
    private String otName;

    public OperationTheatreDto(String otId, String otName) {
        this.otId = otId;
        this.otName = otName;
    }

    public String getOtId() {
        return otId;
    }

    public void setOtId(String otId) {
        this.otId = otId;
    }

    public String getOtName() {
        return otName;
    }

    public void setOtName(String otName) {
        this.otName = otName;
    }
}

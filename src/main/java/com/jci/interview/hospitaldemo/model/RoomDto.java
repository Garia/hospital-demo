package com.jci.interview.hospitaldemo.model;

import java.util.List;

public class RoomDto {

    private String roomId;
    private String roomName;
    private List<PatientDto> patients;

    public void addPatient(PatientDto patient) {
        this.getPatients().add(patient);
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public List<PatientDto> getPatients() {
        return patients;
    }

    public void setPatients(List<PatientDto> patients) {
        this.patients = patients;
    }
}

package com.jci.interview.hospitaldemo.model;

import java.util.List;

public class WardDto {

    private String wardId;
    private String wardName;
    private List<RoomDto> rooms;
    private List<DoctorDto> doctors;
    private List<NurseDto> nurses;
    private List<OperationTheatreDto> operationTheatres;

    public void addRoom(RoomDto room) {
        this.getRooms().add(room);
    }

    public void addNurse(NurseDto nurse) {
        this.getNurses().add(nurse);
    }

    public void addOperationTheatre(OperationTheatreDto operationTheatre) {
        this.getOperationTheatres().add(operationTheatre);
    }

    public void addDoctor(DoctorDto doctor) {
        this.getDoctors().add(doctor);
    }

    public String getWardId() {
        return wardId;
    }

    public void setWardId(String wardId) {
        this.wardId = wardId;
    }

    public String getWardName() {
        return wardName;
    }

    public void setWardName(String wardName) {
        this.wardName = wardName;
    }

    public List<RoomDto> getRooms() {
        return rooms;
    }

    public void setRooms(List<RoomDto> rooms) {
        this.rooms = rooms;
    }

    public List<NurseDto> getNurses() {
        return nurses;
    }

    public void setNurses(List<NurseDto> nurses) {
        this.nurses = nurses;
    }

    public List<OperationTheatreDto> getOperationTheatres() {
        return operationTheatres;
    }

    public void setOperationTheatres(List<OperationTheatreDto> operationTheatres) {
        this.operationTheatres = operationTheatres;
    }

    public List<DoctorDto> getDoctors() {
        return doctors;
    }

    public void setDoctors(List<DoctorDto> doctors) {
        this.doctors = doctors;
    }
}

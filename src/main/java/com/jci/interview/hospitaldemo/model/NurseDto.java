package com.jci.interview.hospitaldemo.model;

public class NurseDto {

    private String nurseId;
    private String nurseName;

    public NurseDto(String nurseId, String nurseName) {
        this.nurseId = nurseId;
        this.nurseName = nurseName;
    }

    public String getNurseId() {
        return nurseId;
    }

    public void setNurseId(String nurseId) {
        this.nurseId = nurseId;
    }

    public String getNurseName() {
        return nurseName;
    }

    public void setNurseName(String nurseName) {
        this.nurseName = nurseName;
    }
}

package com.jci.interview.hospitaldemo.repository;

import com.jci.interview.hospitaldemo.domain.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Repository
public class HospitalRepositoryImpl implements HospitalRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(HospitalRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Patient> getPatientsInRoom(String wardId, String roomId) {
        LOGGER.info("Retrieving patients data for ward {} and room {} from database", wardId, roomId);
        TypedQuery<Patient> query = entityManager.createQuery("SELECT patient FROM Patient patient " +
                "WHERE patient.room.roomId=:roomId AND patient.room.ward.wardId=:wardId", Patient.class);
        query.setParameter("wardId", wardId);
        query.setParameter("roomId", roomId);
        return query.getResultList();
    }

    @Override
    public List<Doctor> getDoctorsInWard(String wardId) {
        LOGGER.info("Retrieving doctors data for ward {} from database", wardId);
        TypedQuery<Doctor> query = entityManager.createQuery("SELECT doctor FROM Doctor doctor join doctor.wards wards WHERE wards.wardId=:wardId", Doctor.class);
        query.setParameter("wardId", wardId);
        return query.getResultList();
    }

    @Override
    public List<Nurse> getNursesInWard(String wardId) {
        LOGGER.info("Retrieving nurses data for ward {} from database", wardId);
        TypedQuery<Nurse> query = entityManager.createQuery("SELECT nurse from Nurse nurse WHERE nurse.ward.wardId=:wardId", Nurse.class);
        query.setParameter("wardId", wardId);
        return query.getResultList();
    }

    @Override
    public List<OperationTheatre> getOperationTheatresInWard(String wardId) {
        LOGGER.info("Retrieving operation theatres data for ward {} from database", wardId);
        TypedQuery<OperationTheatre> query = entityManager.createQuery("SELECT ot from OperationTheatre ot WHERE ot.ward.wardId=:wardId", OperationTheatre.class);
        query.setParameter("wardId", wardId);
        return query.getResultList();
    }

    @Override
    public PatientChart getPatientChart(String patientId) {
        LOGGER.info("Retrieving patient chart data for ward {} from database", patientId);
        TypedQuery<PatientChart> query = entityManager.createQuery("SELECT chart from PatientChart chart WHERE chart.patient.patientId=:patientId", PatientChart.class);
        query.setParameter("patientId", patientId);
        return query.getSingleResult();
    }

    @Transactional
    @Override
    public void saveHospitalData(Hospital hospital) {
        LOGGER.info("Persisted sample hospital data");
        entityManager.persist(hospital);
    }
}

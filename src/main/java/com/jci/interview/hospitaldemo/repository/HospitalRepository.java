package com.jci.interview.hospitaldemo.repository;

import com.jci.interview.hospitaldemo.domain.*;

import java.util.List;

public interface HospitalRepository {

    List<Patient> getPatientsInRoom(String wardId, String roomId);

    List<Doctor> getDoctorsInWard(String wardId);

    List<Nurse> getNursesInWard(String wardId);

    List<OperationTheatre> getOperationTheatresInWard(String wardId);

    PatientChart getPatientChart(String patientId);

    void saveHospitalData(Hospital hospital);
}

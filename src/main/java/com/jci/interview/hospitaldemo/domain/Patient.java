package com.jci.interview.hospitaldemo.domain;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String patientName;

    @Column
    private String patientId;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "Doctor_Patient",
            joinColumns =
                    { @JoinColumn(name = "patientId")},
            inverseJoinColumns =
                    { @JoinColumn(name = "doctorId")})
    private Set<Doctor> doctors;

    @OneToOne(mappedBy = "patient", cascade = CascadeType.ALL)
    private PatientChart patientChart;

    @ManyToOne
    @JoinColumn(name = "roomId")
    private Room room;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public Set<Doctor> getDoctors() {
        return doctors;
    }

    public void setDoctors(Set<Doctor> doctors) {
        this.doctors = doctors;
    }

    public PatientChart getPatientChart() {
        return patientChart;
    }

    public void setPatientChart(PatientChart patientChart) {
        this.patientChart = patientChart;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}

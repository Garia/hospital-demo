package com.jci.interview.hospitaldemo.domain;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Ward {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String wardId;

    @Column
    private String wardName;

    @ManyToOne
    @JoinColumn(name = "hospitalId")
    private Hospital hospital;

    @OneToMany(mappedBy = "ward", cascade = CascadeType.ALL)
    //To avoid org.hibernate.loader.MultipleBagFetchException: cannot simultaneously fetch multiple bags
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Room> rooms;

    @OneToMany(mappedBy = "ward", cascade = CascadeType.ALL)
    //To avoid org.hibernate.loader.MultipleBagFetchException: cannot simultaneously fetch multiple bags
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<OperationTheatre> operationTheatres;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "Ward_Doctor",
            joinColumns =
                    { @JoinColumn(name = "wardId")},
            inverseJoinColumns =
                    { @JoinColumn(name = "doctorId")})
    private Set<Doctor> doctors = new HashSet<>();

    @OneToMany(mappedBy = "ward", cascade = CascadeType.ALL)
    //To avoid org.hibernate.loader.MultipleBagFetchException: cannot simultaneously fetch multiple bags
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Nurse> nurses;

    public void addDoctor(Doctor doctor) {
        this.doctors.add(doctor);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getWardId() {
        return wardId;
    }

    public void setWardId(String wardId) {
        this.wardId = wardId;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public String getWardName() {
        return wardName;
    }

    public void setWardName(String wardName) {
        this.wardName = wardName;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public List<OperationTheatre> getOperationTheatres() {
        return operationTheatres;
    }

    public void setOperationTheatres(List<OperationTheatre> operationTheatres) {
        this.operationTheatres = operationTheatres;
    }

    public Set<Doctor> getDoctors() {
        return doctors;
    }

    public void setDoctors(Set<Doctor> doctors) {
        this.doctors = doctors;
    }

    public List<Nurse> getNurses() {
        return nurses;
    }

    public void setNurses(List<Nurse> nurses) {
        this.nurses = nurses;
    }
}

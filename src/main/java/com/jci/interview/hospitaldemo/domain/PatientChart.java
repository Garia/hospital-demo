package com.jci.interview.hospitaldemo.domain;

import javax.persistence.*;

@Entity
public class PatientChart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String chartName;

    @Column
    private String chartDescription;

    @OneToOne
    @JoinColumn(name = "patientId")
    private Patient patient;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getChartName() {
        return chartName;
    }

    public void setChartName(String chartName) {
        this.chartName = chartName;
    }

    public String getChartDescription() {
        return chartDescription;
    }

    public void setChartDescription(String chartDescription) {
        this.chartDescription = chartDescription;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}

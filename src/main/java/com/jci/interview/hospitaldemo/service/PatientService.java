package com.jci.interview.hospitaldemo.service;

import com.jci.interview.hospitaldemo.model.PatientChartDto;

import java.util.List;

public interface PatientService {

    PatientChartDto getPatientChart(String patientId);
}

package com.jci.interview.hospitaldemo.service;

import com.jci.interview.hospitaldemo.domain.Doctor;
import com.jci.interview.hospitaldemo.domain.Nurse;
import com.jci.interview.hospitaldemo.domain.OperationTheatre;
import com.jci.interview.hospitaldemo.domain.Patient;
import com.jci.interview.hospitaldemo.model.DoctorDto;
import com.jci.interview.hospitaldemo.model.NurseDto;
import com.jci.interview.hospitaldemo.model.OperationTheatreDto;
import com.jci.interview.hospitaldemo.model.PatientDto;
import com.jci.interview.hospitaldemo.repository.HospitalRepository;
import com.jci.interview.hospitaldemo.transformer.EntityToDtoTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WardServiceImpl implements WardService {

    private static final Logger LOGGER  = LoggerFactory.getLogger(WardServiceImpl.class);

    @Autowired
    private EntityToDtoTransformer entityToDtoTransformer;

    @Autowired
    private HospitalRepository hospitalRepository;

    @Override
    public List<PatientDto> getPatientsInARoom(String wardId, String roomId) {
        List<Patient> patients = hospitalRepository.getPatientsInRoom(wardId, roomId);
        LOGGER.info("Found {} no of patients in the room with id {} and ward with id {}", patients.size(), roomId, wardId);
        return entityToDtoTransformer.transformPatients(patients);
    }

    @Override
    public List<DoctorDto> getDoctorsInAWard(String wardId) {
        List<Doctor> doctors = hospitalRepository.getDoctorsInWard(wardId);
        LOGGER.info("Found {} no of doctors in the ward with id {} ", doctors.size(), wardId);
        return entityToDtoTransformer.transformDoctors(doctors);
    }

    @Override
    public List<NurseDto> getNursesInAWard(String wardId) {
        List<Nurse> nurses = hospitalRepository.getNursesInWard(wardId);
        LOGGER.info("Found {} no of nurses in the ward with id {} ", nurses.size(), wardId);
        return entityToDtoTransformer.transformNurses(nurses);
    }

    @Override
    public List<OperationTheatreDto> getOpTheatresInAWard(String wardId) {
        List<OperationTheatre> operationTheatres = hospitalRepository.getOperationTheatresInWard(wardId);
        LOGGER.info("Found {} no of operation theatres in the ward with id {} ", operationTheatres.size(), wardId);
        return entityToDtoTransformer.transformOperationTheatres(operationTheatres);
    }
}

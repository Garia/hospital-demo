package com.jci.interview.hospitaldemo.service;

import com.jci.interview.hospitaldemo.domain.PatientChart;
import com.jci.interview.hospitaldemo.model.PatientChartDto;
import com.jci.interview.hospitaldemo.repository.HospitalRepository;
import com.jci.interview.hospitaldemo.transformer.EntityToDtoTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientServiceImpl implements PatientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientServiceImpl.class);

    @Autowired
    private EntityToDtoTransformer entityToDtoTransformer;

    @Autowired
    private HospitalRepository hospitalRepository;

    @Override
    public PatientChartDto getPatientChart(String patientId) {
        PatientChart patientChart = hospitalRepository.getPatientChart(patientId);
        LOGGER.info("Retrieved patient chart for patient with id {}", patientId);
        return entityToDtoTransformer.transform(patientChart);
    }
}

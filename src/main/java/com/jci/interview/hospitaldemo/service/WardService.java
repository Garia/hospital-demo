package com.jci.interview.hospitaldemo.service;

import com.jci.interview.hospitaldemo.model.DoctorDto;
import com.jci.interview.hospitaldemo.model.NurseDto;
import com.jci.interview.hospitaldemo.model.OperationTheatreDto;
import com.jci.interview.hospitaldemo.model.PatientDto;

import java.util.List;

public interface WardService {

    List<PatientDto> getPatientsInARoom(String wardId, String roomId);

    List<DoctorDto> getDoctorsInAWard(String wardId);

    List<NurseDto> getNursesInAWard(String wardId);

    List<OperationTheatreDto> getOpTheatresInAWard(String wardId);
}

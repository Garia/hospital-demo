package com.jci.interview.hospitaldemo;

import com.jci.interview.hospitaldemo.domain.*;
import com.jci.interview.hospitaldemo.repository.HospitalRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Not required for the functioning of the application, it
 * is only required for populating the database with sample data for testing
 * the request endpoints.
 */
@Component
public class PopulateDataComponent implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(PopulateDataComponent.class);

    @Autowired
    private HospitalRepository hospitalRepository;

    @Override
    public void run(String... args) throws Exception {
        logger.info("Inserting sample data now in the database............");

        Hospital hospital = buildSampleData();
        hospitalRepository.saveHospitalData(hospital);
    }

    public Hospital buildSampleData() {
        // Hospital
        Hospital hospital = new Hospital();
        hospital.setHospitalName("Manisha's Hospital");
        hospital.setHospitalId("id-01");

        // List of wards ina a hospital
        List<Ward> wards = new ArrayList<>();

        // Ward 1
        Ward ward1 = new Ward();
        ward1.setWardId("ward-id-1");
        ward1.setWardName("First ward");
        ward1.setHospital(hospital);

        //Rooms for ward 1
        List<Room> rooms1 = new ArrayList<>();

        // Room 1
        Room room1 = new Room();
        room1.setRoomId("room-1");
        room1.setRoomName("room 1 for 1st ward");
        room1.setRoomType("AC");
        room1.setWard(ward1);

        // List of patients for room 1
        List<Patient> patients1_1 = new ArrayList<>();

        //First patient for room 1
        Patient patient1 = getPatient("1", room1);
        patients1_1.add(patient1);
        //Second patient for room 1
        Patient patient2 = getPatient("2", room1);
        patients1_1.add(patient2);

        //Set the patients in room 1
        room1.setPatients(patients1_1);

        rooms1.add(room1);

        // Room 2
        Room room2 = new Room();
        room2.setRoomId("room-2");
        room2.setRoomName("room 2 for 1st ward");
        room2.setRoomType("NON-AC");
        room2.setWard(ward1);

        // List of patients for room 2
        List<Patient> patients1_2 = new ArrayList<>();

        //First patient for room 2
        Patient patient3 = getPatient("3", room2);
        patients1_2.add(patient3);
        //Second patient for room 2
        Patient patient4 = getPatient("4", room2);
        patients1_2.add(patient4);

        //Set the patients in room 1
        room2.setPatients(patients1_2);

        rooms1.add(room2);

        // Set the rooms in the first ward
        ward1.setRooms(rooms1);

        // Operation theatres for Ward 1
        List<OperationTheatre> operationTheatres1 = new ArrayList<>();

        // Operation theatre 1
        OperationTheatre operationTheatre1 = getOperationTheatre("1", ward1);
        operationTheatres1.add(operationTheatre1);

        //Operation theatre 2
        OperationTheatre operationTheatre2 = getOperationTheatre("2", ward1);
        operationTheatres1.add(operationTheatre2);

        // Set both operation theatres in ward 1
        ward1.setOperationTheatres(operationTheatres1);

        // List of nurses in ward 1
        List<Nurse> nurses1 = new ArrayList<>();
        //1st nurse for ward 1
        Nurse nurse1 = getNurse("1", ward1);
        nurses1.add(nurse1);
        //2nd nurse for ward 1
        Nurse nurse2 = getNurse("2", ward1);
        nurses1.add(nurse2);

        ward1.setNurses(nurses1);

        //Get and add doctors to the ward
        ward1.addDoctor(getDoctor("1", ward1));
        ward1.addDoctor(getDoctor("2", ward1));

        wards.add(ward1);

        // Ward 2
        Ward ward2 = new Ward();
        ward2.setWardId("ward-id-2");
        ward2.setWardName("Second ward");
        ward2.setHospital(hospital);

        // List of rooms for ward 2
        List<Room> rooms2 = new ArrayList<>();

        // Room 3
        Room room3 = new Room();
        room3.setRoomId("room-3");
        room3.setRoomName("room 1 for 2nd ward");
        room3.setRoomType("AC");
        room3.setWard(ward2);
        rooms2.add(room3);

        // Room 4
        Room room4 = new Room();
        room4.setRoomId("room-4");
        room4.setRoomName("room 2 for 2nd ward");
        room4.setRoomType("NON-AC");
        room4.setWard(ward2);
        rooms2.add(room4);

        //Set the rooms for ward 2
        ward2.setRooms(rooms2);

        // Operation theatres for Ward 2
        List<OperationTheatre> operationTheatres2 = new ArrayList<>();

        // Operation theatre 3
        OperationTheatre operationTheatre3 = getOperationTheatre("3", ward2);
        operationTheatres2.add(operationTheatre3);

        //Operation theatre 4
        OperationTheatre operationTheatre4 = getOperationTheatre("4", ward2);
        operationTheatres2.add(operationTheatre4);

        // Set both operation theatres in ward 2
        ward2.setOperationTheatres(operationTheatres2);

        // List of nurses in ward 2
        List<Nurse> nurses2 = new ArrayList<>();
        //1st nurse for ward 2
        Nurse nurse3 = getNurse("3", ward2);
        nurses2.add(nurse3);
        //2nd nurse for ward 2
        Nurse nurse4 = getNurse("4", ward2);
        nurses2.add(nurse4);

        ward2.setNurses(nurses2);

        //Get and add doctors to the ward
        ward2.addDoctor(getDoctor("3", ward2));
        ward2.addDoctor(getDoctor("4", ward2));
        ward2.addDoctor(getDoctor("5", ward2));
        ward2.addDoctor(getDoctor("6", ward2));

        wards.add(ward2);

        hospital.setWards(wards);
        return hospital;
    }

    private Patient getPatient(String pno, Room room) {
        Patient patient = new Patient();
        patient.setPatientId("patient-id-" +pno);
        patient.setPatientName(pno + " patient-name");
        patient.setPatientChart(getPatientChart(pno, patient));
        patient.setRoom(room);
        return patient;
    }

    private PatientChart getPatientChart(String pchartNo, Patient patient) {
        PatientChart patientChart = new PatientChart();

        patientChart.setChartName(pchartNo + "-patient-chart-name");
        patientChart.setChartDescription(pchartNo + "-patient-chart-description");
        patientChart.setPatient(patient);

        return patientChart;
    }

    private Nurse getNurse(String nurseNo, Ward ward) {
        Nurse nurse = new Nurse();
        nurse.setNurseId(nurseNo + "-nurse-id");
        nurse.setNurseName(nurseNo + "-nurse-name");
        nurse.setWard(ward);
        return nurse;
    }

    private OperationTheatre getOperationTheatre(String otNo, Ward ward) {
        OperationTheatre operationTheatre = new OperationTheatre();
        operationTheatre.setTheatreName(otNo + "-ot-name");
        operationTheatre.setTheatreId(otNo + "-ot-id");
        operationTheatre.setWard(ward);
        return operationTheatre;
    }

    private Doctor getDoctor(String doctorNo, Ward ward) {
        Doctor doctor = new Doctor();
        doctor.setDoctorName(doctorNo + "-doctor-name");
        doctor.setDoctorId(doctorNo + "-doctor-id");
        doctor.addWard(ward);
        return doctor;
    }


}

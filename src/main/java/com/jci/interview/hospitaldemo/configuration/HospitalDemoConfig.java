package com.jci.interview.hospitaldemo.configuration;

import com.jci.interview.hospitaldemo.transformer.EntityToDtoTransformer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HospitalDemoConfig {

    @Bean
    public EntityToDtoTransformer entityToDtoTransformer() {
        return new EntityToDtoTransformer();
    }
}

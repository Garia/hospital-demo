package com.jci.interview.hospitaldemo.transformer;

import com.jci.interview.hospitaldemo.domain.*;
import com.jci.interview.hospitaldemo.model.*;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EntityToDtoTransformer {

    //Transforms the patient chart entity class to PatientChartDto
    public PatientChartDto transform(PatientChart patientChart) {
        if (patientChart == null) {
            return new PatientChartDto();
        }
        return new PatientChartDto(patientChart.getChartDescription(), patientChart.getChartName());
    }

    //Transform the Patients entity to DTO list
    public List<PatientDto> transformPatients(List<Patient> patients) {
        if (CollectionUtils.isEmpty(patients)) {
            return new ArrayList<>();
        }
        final List<PatientDto> patientDtos = new ArrayList<>();
        patients.stream().filter(Objects::nonNull).forEach(p -> patientDtos.add(new PatientDto(p.getPatientId(), p.getPatientName())));
        return patientDtos;
    }

    //Transform the Doctors entity to DTO list
    public List<DoctorDto> transformDoctors(List<Doctor> doctors) {
        if (CollectionUtils.isEmpty(doctors)) {
            return new ArrayList<>();
        }
        final List<DoctorDto> doctorDtos = new ArrayList<>();
        doctors.stream().filter(Objects::nonNull).forEach(d -> doctorDtos.add(new DoctorDto(d.getDoctorId(), d.getDoctorName())));
        return doctorDtos;
    }

    //Transform the Nurses entity to DTO list
    public List<NurseDto> transformNurses(List<Nurse> nurses) {
        if (CollectionUtils.isEmpty(nurses)) {
            return new ArrayList<>();
        }
        final List<NurseDto> nurseDtos = new ArrayList<>();
        nurses.stream().filter(Objects::nonNull).forEach(n -> nurseDtos.add(new NurseDto(n.getNurseId(), n.getNurseName())));
        return nurseDtos;
    }

    //Transform the Operation Theatres entity to DTO list
    public List<OperationTheatreDto> transformOperationTheatres(List<OperationTheatre> operationTheatres) {
        if (CollectionUtils.isEmpty(operationTheatres)) {
            return new ArrayList<>();
        }
        final List<OperationTheatreDto> operationTheatreDtos = new ArrayList<>();
        operationTheatres.stream().filter(Objects::nonNull).forEach(ot -> operationTheatreDtos.add(new OperationTheatreDto(ot.getTheatreId(), ot.getTheatreName())));
        return operationTheatreDtos;
    }
}

package com.jci.interview.hospitaldemo.controller;

import com.jci.interview.hospitaldemo.model.PatientChartDto;
import com.jci.interview.hospitaldemo.service.PatientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/patients")
public class PatientController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientController.class);

    @Autowired
    private PatientService patientService;

    @RequestMapping(value = "/{patientId}/chart", method = RequestMethod.GET)
    @ResponseBody
    public PatientChartDto getPatientChart(@PathVariable String patientId) {
        LOGGER.info("PatientID is {}", patientId);
        return patientService.getPatientChart(patientId);
    }
}

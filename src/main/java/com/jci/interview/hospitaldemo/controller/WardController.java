package com.jci.interview.hospitaldemo.controller;

import com.jci.interview.hospitaldemo.model.DoctorDto;
import com.jci.interview.hospitaldemo.model.NurseDto;
import com.jci.interview.hospitaldemo.model.OperationTheatreDto;
import com.jci.interview.hospitaldemo.model.PatientDto;
import com.jci.interview.hospitaldemo.service.WardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/wards")
public class WardController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WardController.class);

    @Autowired
    private WardService wardService;

    @RequestMapping(value = "/{wardId}/rooms/{roomId}/patients", method = RequestMethod.GET)
    @ResponseBody
    public List<PatientDto> getPatients(@PathVariable String wardId, @PathVariable String roomId) {
        LOGGER.info("WardID is {} and roomID is {}", wardId, roomId);
        return wardService.getPatientsInARoom(wardId, roomId);
    }

    @RequestMapping(value = "/{wardId}/doctors", method = RequestMethod.GET)
    @ResponseBody
    public List<DoctorDto> getDoctors(@PathVariable String wardId) {
        LOGGER.info("WardID is {}", wardId);
        return wardService.getDoctorsInAWard(wardId);
    }

    @RequestMapping(value = "/{wardId}/nurses", method = RequestMethod.GET)
    @ResponseBody
    public List<NurseDto> getNurses(@PathVariable String wardId) {
        LOGGER.info("WardID is {}", wardId);
        return wardService.getNursesInAWard(wardId);
    }

    @RequestMapping(value = "/{wardId}/theatres", method = RequestMethod.GET)
    @ResponseBody
    public List<OperationTheatreDto> getOpTheatres(@PathVariable String wardId) {
        LOGGER.info("WardID is {}", wardId);
        return wardService.getOpTheatresInAWard(wardId);
    }
}
